import 'dart:async';

import 'package:flutter/material.dart';

import 'puzzlepiece.dart';

class Game extends StatelessWidget {
  var index = -1;
  Game(index) {
    this.index = index;
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: MyGamePage(this.index, UniqueKey()),
    );
  }
}

class MyGamePage extends StatefulWidget {
  final int rows = 4;
  final int cols = 4;
  int index;
  MyGamePage(int index, Key key) : super(key: key) {
    this.index = index;
  }

  @override
  _MyGamePageState createState() => _MyGamePageState(this.index);
}

class _MyGamePageState extends State<MyGamePage> {
  Image _image;
  List<Widget> pieces = [];
  int index;
  _MyGamePageState(index) {
    this.index = index;
    this.getImage();
  }
  Future<Size> getImageSize(Image image) async {
    final Completer<Size> completer = Completer<Size>();

    image.image
        .resolve(const ImageConfiguration())
        .addListener(ImageStreamListener(
      (ImageInfo info, bool _) {
        completer.complete(Size(
          info.image.width.toDouble(),
          info.image.height.toDouble(),
        ));
      },
    ));

    final Size imageSize = await completer.future;

    return imageSize;
  }

  Future getImage() async {
    var image = new Image(
        image:
            AssetImage('assets/images/img' + this.index.toString() + '.jpg'));
    if (image.image != null) {
      
        _image = image;
        pieces.clear();

      splitImage(image);
    }
  }

  void splitImage(Image image) async {
    Size imageSize = await getImageSize(image);

    for (int x = 0; x < widget.rows; x++) {
      for (int y = 0; y < widget.cols; y++) {
        setState(() {
          pieces.add(PuzzlePiece(
              key: GlobalKey(),
              image: image,
              imageSize: imageSize,
              row: x,
              col: y,
              maxRow: widget.rows,
              maxCol: widget.cols,
              bringToTop: this.bringToTop,
              sendToBack: this.sendToBack));
        });
      }
    }
  }

// when the pan of a piece starts, we need to bring it to the front of the stack
  void bringToTop(Widget widget) {
    setState(() {
      pieces.remove(widget);
      pieces.add(widget);
    });
  }

// when a piece reaches its final position, it will be sent to the back of the stack to not get in the way of other, still movable, pieces
  void sendToBack(Widget widget) {
    setState(() {
      pieces.remove(widget);
      pieces.insert(0, widget);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: SafeArea(
          maintainBottomViewPadding: true,
          child: _image == null
              ? new Text('Nincs kép kiválasztva.')
              : Stack(children: pieces)),
    );
  }
}
